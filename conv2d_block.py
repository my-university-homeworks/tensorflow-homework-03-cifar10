import tensorflow as tf
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, GlobalMaxPooling2D, Layer

class Conv2DBlock(Layer):
    def __init__(self, layers, filters, kernel_size=3, activation=tf.nn.relu):
        super(Conv2DBlock, self).__init__()
        self.conv_layers = []

        for i in range(layers):
            self.conv_layers.append(Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                padding="same",
                activation=activation,
                name=f"conv2d_{kernel_size}x{filters}_{i + 1}"
            ))

    def call(self, x):
        for conv_layer in self.conv_layers:
            x = conv_layer(x)
        return x

# Example Usage:
input_shape = (64, 64, 3)  # Adjust input shape based on your needs
model = tf.keras.Sequential([
    Conv2DBlock(layers=2, filters=32, kernel_size=3),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2DBlock(layers=2, filters=64, kernel_size=3),
    MaxPooling2D(pool_size=(2, 2)),
    GlobalMaxPooling2D(),
    Dense(units=128, activation='relu'),
    Dense(units=10, activation='softmax')
])

# Compile and fit the model as needed
