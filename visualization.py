from typing import Optional
import os

import matplotlib.pyplot as plt

def visualize(train_losses, train_accuracies, test_losses, test_accuracies, name: Optional[str] = None,
              save_directory: Optional[str] = "./plots/", display_plot: Optional[bool] = True):
    """
    Plot the received metrics.

    Args:
        train_losses (list): List of training losses.
        train_accuracies (list): List of training accuracies.
        test_losses (list): List of test losses.
        test_accuracies (list): List of test accuracies.
        name (str, optional): Name for the plot. Defaults to None.
        save_directory (str, optional): Directory to save the plot. Defaults to "./plots/".
        display_plot (bool, optional): Whether to display the plot. Defaults to True.
    """

    fig, ax = plt.subplots()
    plt.title("Loss and Accuracy over Training Epochs")
    plt.xlabel("Training Epoch")

    # Plotting average training and test losses
    train_loss_line, = ax.plot(train_losses, 'b:', label='Train Loss')
    test_loss_line, = ax.plot(test_losses, 'b-', label='Test Loss')
    ax.tick_params(axis='y', labelcolor='blue')
    ax.set_ylabel('Loss', color='blue')

    # Creating a second y-axis for accuracy
    ax2 = ax.twinx()
    train_acc_line, = ax2.plot(train_accuracies, 'g:', label='Train Accuracy (%)')
    test_acc_line, = ax2.plot(test_accuracies, 'g-', label='Test Accuracy (%)')
    ax2.tick_params(axis='y', labelcolor='green')
    ax2.set_ylabel('Accuracy', color='green')

    # Adding a single legend for both axes
    lines = [train_loss_line, test_loss_line, train_acc_line, test_acc_line]
    labels = ['Avg. Train Loss', 'Avg. Test Loss', 'Train Accuracy (%)', 'Test Accuracy (%)']
    ax.legend(lines, labels, loc='upper left')

    # Save the plot if a name is provided
    if name:
        try:
            if not os.path.exists(save_directory):
                os.makedirs(save_directory)
            plt.savefig(os.path.join(save_directory, f"{name}.png"))
        except Exception as e:
            print(f"Error saving the plot: {e}")

    # Display the plot if specified
    if display_plot:
        try:
            plt.show()
        except ConnectionRefusedError:
            print("Unable to display the plot.")
