import tensorflow as tf
from tensorflow.keras.layers import Dense, GlobalMaxPooling2D, MaxPooling2D
from conv2d_block import Conv2DBlock
import config

class MyModel(tf.keras.Model):
    def __init__(self):
        super(MyModel, self).__init__()

        # Use a Sequential model for simplicity and readability
        self.features = tf.keras.Sequential([
            Conv2DBlock(2, 32),
            MaxPooling2D(),
            Conv2DBlock(2, 64),
            GlobalMaxPooling2D()
        ])

        self.classifier = tf.keras.Sequential([
            Dense(24, activation=tf.nn.relu),
            Dense(10, activation=tf.nn.softmax)
        ])

        self.optimizer = tf.keras.optimizers.SGD(learning_rate=config.LEARNING_RATE)
        self.metrics_list = [
            tf.keras.metrics.Mean(name="loss"),
            tf.keras.metrics.CategoricalAccuracy(name="accuracy"),
        ]
        self.loss_function = tf.keras.losses.CategoricalCrossentropy(from_logits=True)

    @tf.function
    def call(self, x, training=False):
        x = self.features(x, training=training)
        return self.classifier(x)

    def reset_metrics(self):
        for metric in self.metrics_list:
            metric.reset_states()

    @tf.function
    def train_step(self, data):
        x, targets = data

        with tf.GradientTape() as tape:
            predictions = self(x, training=True)

            loss = self.loss_function(targets, predictions) + tf.reduce_sum(self.losses)

        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        # Update loss metric
        self.metrics_list[0].update_state(loss)

        # For all metrics except loss, update states (accuracy etc.)
        for metric in self.metrics_list[1:]:
            metric.update_state(targets, predictions)

    @tf.function
    def test_step(self, data):
        x, targets = data
        predictions = self(x, training=False)
        loss = self.loss_function(targets, predictions) + tf.reduce_sum(self.losses)

        # Update loss metric
        self.metrics_list[0].update_state(loss)

        # For accuracy metrics
        for metric in self.metrics_list[1:]:
            metric.update_state(targets, predictions)
